<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Default:index.html.twig', array());
    }
	
	public function adminAction()
	{
		return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin:index.html.twig', array());
	}
	
	public function pageAction($pagename)
	{
		$page = $this->getDoctrine()
			->getRepository('MelvinLoosProjectTemplatesBasicBundle:Page')
			->findOneByName($pagename);
			
		if (!$page)
		{
			throw $this->createNotFoundException("No page found for '".$pagename."'");
		}
		
		return $this->render('MelvinLoosProjectTemplatesBasicBundle:Default:page.html.twig', array('page' => $page));
	}
}
