<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Controller\Menu;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node;
use MelvinLoos\ProjectTemplates\BasicBundle\Form\Menu\NodeType;

/**
 * Menu\Node controller.
 *
 */
class NodeController extends Controller
{
    /**
     * Lists all Menu\Node entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MelvinLoosProjectTemplatesBasicBundle:Menu\Node')->findAll();

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Menu\Node entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Node();
        $form = $this->createForm(new NodeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
        	$entity->setCreatedBy('User');
			if ($entity->getUrl() == '')
			{
				$entity->setUrl($this->get('router')->generate('page', array('pagename' => $this->makeUrl($entity->getPage())), true));
			}
			else
			{
				try
				{
					$route = $this->get('router')->match($entity->getUrl());
					$page = $this->getDoctrine()
						->getRepository('MelvinLoosProjectTemplatesBasicBundle:Page')
						->findOneByName($route['pagename']);
					if ($page != $entity->getPage())
					{
						$entity->setPage($page);
					}
				}
				catch(Exception $e)
				{
					
				}
			}
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('menu_node_show', array('id' => $entity->getId())));
        }

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Menu\Node entity.
     *
     */
    public function newAction()
    {
        $entity = new Node();
        $form   = $this->createForm(new NodeType(), $entity);

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Menu\Node entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MelvinLoosProjectTemplatesBasicBundle:Menu\Node')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu\Node entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Menu\Node entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MelvinLoosProjectTemplatesBasicBundle:Menu\Node')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu\Node entity.');
        }

        $editForm = $this->createForm(new NodeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Menu\Node entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MelvinLoosProjectTemplatesBasicBundle:Menu\Node')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu\Node entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new NodeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
        	if ($entity->getUrl() == '')
			{
				$entity->setUrl($this->get('router')->generate('page', array('pagename' => $this->makeUrl($entity->getPage())), true));
			}
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('menu_node_edit', array('id' => $id)));
        }

        return $this->render('MelvinLoosProjectTemplatesBasicBundle:Admin\Menu\Node:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Menu\Node entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MelvinLoosProjectTemplatesBasicBundle:Menu\Node')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Menu\Node entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('menu_node'));
    }

    /**
     * Creates a form to delete a Menu\Node entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	/**
	 * transform a string into a url-ready string
	 */
	private function makeUrl($input)
	{
		// first make everything lowercase
		$output = strtolower($input);
		
		return $output;
	}
}