<?php
namespace MelvinLoos\ProjectTemplates\BasicBundle\Entity\Base;

use Doctrine\ORM\Mapping as ORM;

/**
 * This is a abstract class with the basic properties to make an object traceable.
 * Allowing to keep track of: 
 * 			- when it was created, 
 * 			- who created it, 
 * 			- when it was changed, 
 * 			- who changed it and 
 * 			- what kind of change was done.
 * 
 * @ORM\MappedSuperclass
 */
abstract class Traceable
{
	/**
	 * @var /DateTime
	 * 
	 * @ORM\Column(name="created_on", type="datetime")
	 */
	protected $createdOn;
	
	/**
	 * @var string
	 * 
	 * @ORM\Column(name="created_by", type="string", nullable=true)
	 */
	protected $createdBy;
	
	/**
	 * @var /DateTime
	 * 
	 * @ORM\Column(name="changed_on", type="datetime", nullable=true)
	 */
	protected $changedOn;
	
	/**
	 * @var string
	 * 
	 * @ORM\Column(name="changed_by", type="string", nullable=true)
	 */
	protected $changedBy;
	
	/**
	 * @var string
	 * 
	 * Main types are: EDIT, DELETE, OWNER
	 * 
	 * @ORM\Column(name="type_of_change", type="string", length=8, nullable=true)
	 */
	protected $typeOfChange;
	
	/**
	 * Constructor
	 */
	 public function __construct()
	 {
	 	$this->createdOn = new \DateTime();
	 }
	
	/**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Node
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Node
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set changedOn
     *
     * @param \DateTime $changedOn
     * @return Node
     */
    public function setChangedOn($changedOn)
    {
        $this->changedOn = $changedOn;
    
        return $this;
    }

    /**
     * Get changedOn
     *
     * @return \DateTime 
     */
    public function getChangedOn()
    {
        return $this->changedOn;
    }

    /**
     * Set changedBy
     *
     * @param string $changedBy
     * @return Node
     */
    public function setChangedBy($changedBy)
    {
        $this->changedBy = $changedBy;
    
        return $this;
    }

    /**
     * Get changedBy
     *
     * @return string 
     */
    public function getChangedBy()
    {
        return $this->changedBy;
    }

    /**
     * Set typeOfChange
     *
     * @param string $typeOfChange
     * @return Node
     */
    public function setTypeOfChange($typeOfChange)
    {
        $this->typeOfChange = $typeOfChange;
    
        return $this;
    }

    /**
     * Get typeOfChange
     *
     * @return string 
     */
    public function getTypeOfChange()
    {
        return $this->typeOfChange;
    }
}