<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use MelvinLoos\ProjectTemplates\BasicBundle\Entity\Base\Traceable;
use MelvinLoos\ProjectTemplates\BasicBundle\Entity\Page;

/**
 * This class defines the nodes of the menu, they can link to a site page ($page) or to an external url ($link) 
 *
 * @ORM\Table(name="menu_nodes")
 * @ORM\Entity(repositoryClass="MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\NodeRepository")
 */
class Node extends Traceable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @var MelvinLoos\ProjectTemplates\BasicBundle\Entity\Page;
     *
     * @ORM\OneToOne(targetEntity="MelvinLoos\ProjectTemplates\BasicBundle\Entity\Page", mappedBy="node")
	 * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var Node
     *
     * @ORM\ManyToOne(targetEntity="Node", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;
	
	/**
     * @ORM\OneToMany(targetEntity="Node", mappedBy="parent")
     */
	private $children;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Node
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set page
     *
     * @param \stdClass $page
     * @return Node
     */
    public function setPage($page)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page
     *
     * @return \stdClass 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Node
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set parent
     *
     * @param \stdClass $parent
     * @return Node
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \stdClass 
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    	parent::__construct();
		
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();		
    }
    
    /**
     * Add children
     *
     * @param \MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $children
     * @return Node
     */
    public function addChildren(\MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $children)
    {
        $this->children[] = $children;
    
        return $this;
    }

    /**
     * Remove children
     *
     * @param \MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $children
     */
    public function removeChildren(\MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }
	
	/**
	 * Checks if node has any children, returns TRUE or FALSE
	 */
	public function hasChildren()
	{
		$children = $this->getChildren();
		
		return $children->containsKey(0);
	}
	
	public function __toString()
	{
		return $this->name;
	}
}