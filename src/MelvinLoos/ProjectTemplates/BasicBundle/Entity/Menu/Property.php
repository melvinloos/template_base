<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class defines the properties of the menu
 *
 * @ORM\Table(name="menu_properties")
 * @ORM\Entity
 */
class Property
{
	const VALUETYPE_STRING 		= 0;
	const VALUETYPE_INTEGER 	= 1;
	const VALUETYPE_BOOLEAN		= 2;
	const VALUETYPE_OBJECT 		= 3;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;
	
    /**
     * @var string
     *
     * @ORM\Column(name="type_of_value", type="smallint")
     */
    private $typeOfValue;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;
	
	/**
	 * @var array
	 * 
	 * @ORM\Column(name="possible_values", type="array", nullable=true) 
	 */
	 private $possibleValues;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="default_value", type="string", length=255, nullable=true)
	 */
	 private $defaultValue;
	 
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MenuProperty
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set typeOfValue
     *
     * @param string $typeOfValue
     * @return MenuProperty
     */
    public function setTypeOfValue($typeOfValue)
    {
        $this->typeOfValue = $typeOfValue;
    
        return $this;
    }

    /**
     * Get typeOfValue
     *
     * @return string 
     */
    public function getTypeOfValue()
    {
        return $this->typeOfValue;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return MenuProperty
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set possibleValues
     *
     * @param array $possibleValues
     * @return MenuProperty
     */
    public function setPossibleValues($possibleValues)
    {
        $this->possibleValues = $possibleValues;
    
        return $this;
    }

    /**
     * Get possibleValues
     *
     * @return array 
     */
    public function getPossibleValues()
    {
        return $this->possibleValues;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     * @return MenuProperty
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    
        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string 
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
}