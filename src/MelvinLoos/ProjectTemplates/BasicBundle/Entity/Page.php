<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use MelvinLoos\ProjectTemplates\BasicBundle\Entity\Base\Traceable;
use MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node;

/**
 * Page
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity
 */
class Page extends Traceable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;
	
	/**
	 * @ORM\Column(name="content", type="text")
	 */
	 private $content;
	
	/**
	 * @var 
	 * 
	 * @ORM\OneToOne(targetEntity="MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node", mappedBy="page")
	 * @ORM\JoinColumn(name="node_id", referencedColumnName="id")
	 */
	 private $node;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

	
    public function __toString()
	{
		return $this->name;
	}
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Page
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set node
     *
     * @param \MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $node
     * @return Page
     */
    public function setNode(\MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node $node = null)
    {
        $this->node = $node;
    
        return $this;
    }

    /**
     * Get node
     *
     * @return \MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node 
     */
    public function getNode()
    {
        return $this->node;
    }
}