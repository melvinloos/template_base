<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Form\Menu;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
				'label' => 'Name*'))            
			->add('parent', 'entity', array(
				'class' => 'MelvinLoosProjectTemplatesBasicBundle:Menu\Node',
				'empty_value' => '- Select Parent -',
				'label' => 'Parent (Optional)',
				'required' => false))
            ->add('page')
			->add('customLink', 'choice', array(
				'choices' => array(
					'true' => 'Make It An External Link?'),
				'expanded' => true,
				'multiple' => true,
				'mapped' => false))
			->add('url', 'text', array(
				'read_only' => true))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MelvinLoos\ProjectTemplates\BasicBundle\Entity\Menu\Node'
        ));
    }

    public function getName()
    {
        return 'melvinloos_projecttemplates_basicbundle_menu_nodetype';
    }
}
