<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('content')
            ->add('url')
            ->add('node')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MelvinLoos\ProjectTemplates\BasicBundle\Entity\Page'
        ));
    }

    public function getName()
    {
        return 'melvinloos_projecttemplates_basicbundle_pagetype';
    }
}
