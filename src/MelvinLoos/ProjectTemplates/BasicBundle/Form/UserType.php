<?php

namespace MelvinLoos\ProjectTemplates\BasicBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('suffix')
            ->add('lastName')
            ->add('nickname')
            ->add('department')
            ->add('gender')
            ->add('birthdate')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MelvinLoos\ProjectTemplates\BasicBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'melvinloos_projecttemplates_basicbundle_usertype';
    }
}
