<?php
namespace MelvinLoos\ProjectTemplates\BasicBundle\Twig;

use Twig_Extension;
use Twig_Function_Function;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\FormFactoryInterface;


class BaseExtension extends Twig_Extension
{
	protected $route;
    protected $container;
    protected $context;
    protected $formFactory;
    
    public function __construct(ContainerInterface $container, SecurityContext $context, FormFactoryInterface $formFactory)
    {
    	if (!$container->isScopeActive('request')) {
    		return;	
		}
    	
        $this->container = $container;
        $this->context = $context;
    }

    public function getGlobals()
    {
    	return array(
    		'menu_items' => $this->getMenuNodes()
    	);
    }
    
    public function getFunctions()
    {
        return array(
            
        );
    }
	
    public function getName()
    {
        return 'cms_core_extension';
    }
	
	/**
	 * Function which retrieves all menu nodes at the top level, 
	 * by calling getChildren you can retrieve the rest of the nodes.
	 * 
	 * When no nodes are found it returns false
	 * 
	 */
	private function getMenuNodes()
	{
		$em = $this->container->get('doctrine')->getManager();
		$query = $em->createQuery('SELECT n FROM MelvinLoosProjectTemplatesBasicBundle:Menu\Node n WHERE n.parent IS NULL');
		$menuItems = $query->getResult();
		
		if (empty($menuItems))
		{
			return FALSE;
		}
		
		return $menuItems;
	}
}