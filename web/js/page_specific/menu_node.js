var formElement_customUrl = '#melvinloos_projecttemplates_basicbundle_menu_nodetype_customLink_0';
var formElement_url = '#melvinloos_projecttemplates_basicbundle_menu_nodetype_url';
var formElement_page = '#melvinloos_projecttemplates_basicbundle_menu_nodetype_page';

if ($(formElement_customUrl + ':checkbox:checked').val()) {
	updatePageUrl();
}

$(formElement_customUrl).change(function() {
	if ($(formElement_customUrl + ':checkbox:checked').val()) {
		$(formElement_url).removeAttr('readonly');
		$(formElement_page).attr('readonly', 'readonly');
		if ($(formElement_url).val() == '') {
			$(formElement_url).val('http://');
		}
	} else {
		$(formElement_page).removeAttr('readonly');
		$(formElement_url).attr('readonly', 'readonly');
		updatePageUrl();
	}
});

$(formElement_page).change(function() {
	if ($(formElement_customUrl + ':checkbox:checked').val() == false) {
		updatePageUrl();
	}
});

function updatePageUrl() {
	$(formElement_url).val('');
	//		$(formElement_url).val($(formElement_page).val().toLowerCase());
}